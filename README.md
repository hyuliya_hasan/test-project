Git global setup

git config --global user.name "Hyuliya Hasan"
git config --global user.email "hulyahasan00@gmail.com"

Create a new repository

git clone https://hyuliya_hasan@gitlab.com/hyuliya_hasan/test-project.git
cd test-project
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://hyuliya_hasan@gitlab.com/hyuliya_hasan/test-project.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://hyuliya_hasan@gitlab.com/hyuliya_hasan/test-project.git
git push -u origin --all
git push -u origin --tags